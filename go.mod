module gitlab.com/clusterworks/sshtunneld

go 1.18

require golang.org/x/crypto v0.0.0-20220525230936-793ad666bf5e

require (
	github.com/go-logr/logr v1.2.3
	golang.org/x/sys v0.0.0-20210809222454-d867a43fc93e // indirect
)
