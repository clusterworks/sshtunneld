module gitlab.com/clusterworks/sshtunneld/cmd

go 1.18

require (
	github.com/go-logr/logr v1.2.3
	github.com/go-logr/zerologr v1.2.2
	github.com/rs/zerolog v1.27.0
	gitlab.com/clusterworks/sshtunneld v0.0.0-20220619103237-03565771f104
)

require (
	github.com/mattn/go-colorable v0.1.12 // indirect
	github.com/mattn/go-isatty v0.0.14 // indirect
	golang.org/x/crypto v0.0.0-20220525230936-793ad666bf5e // indirect
	golang.org/x/sys v0.0.0-20210927094055-39ccf1dd6fa6 // indirect
)

replace gitlab.com/clusterworks/sshtunneld => ../
