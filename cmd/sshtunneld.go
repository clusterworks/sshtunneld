package main

import (
	"flag"
	"os"

	"github.com/go-logr/logr"
	"github.com/go-logr/zerologr"
	"github.com/rs/zerolog"

	"gitlab.com/clusterworks/sshtunneld"
)

var (
	listen            = flag.String("listen", ":4444", "Addr to listen on for incoming ssh connections")
	hostKeyPath       = flag.String("hostkey", "sshtunneld", "Server host key to load")
	authorizedKeyPath = flag.String("authorizedkeys", "authorized_keys", "the allowed public keys to login")
	verbose           = flag.Int("verbose", 0, "Enable verbose mode 0,1,2")
)

func main() {
	flag.Parse()

	zerolog.TimeFieldFormat = zerolog.TimeFormatUnixMs
	zerolog.SetGlobalLevel(zerolog.Level(*verbose))
	zerologr.NameFieldName = "logger"
	zerologr.NameSeparator = "/"

	zl := zerolog.New(os.Stderr)
	zl = zl.With().Caller().Timestamp().Logger().Output(zerolog.ConsoleWriter{Out: os.Stdout})
	var log logr.Logger = zerologr.New(&zl)

	tunnel, err := sshtunneld.NewWithKeyFiles(log.WithName("sshtunneld"), *hostKeyPath, *authorizedKeyPath, nil)
	if err != nil {
		log.Error(err, "could not create sshtunneld")
		os.Exit(1)
	}
	tunnel.Listen("tcp", *listen)
}
