package sshtunneld

import (
	"fmt"
	"io/ioutil"
	"strings"

	"golang.org/x/crypto/ssh"
)

func (t *SSHTunneld) publicKeyCallback(conn ssh.ConnMetadata, key ssh.PublicKey) (*ssh.Permissions, error) {
	t.authmutex.Lock()
	defer t.authmutex.Unlock()
	if clientinfo, found := t.authorisedKeys[string(key.Marshal())]; found {
		return &ssh.Permissions{
			Extensions: map[string]string{"name": clientinfo.Name, "groups": strings.Join(clientinfo.Groups, ",")},
		}, nil
	}

	return nil, fmt.Errorf("unknown public key")
}

func AllowAny(conn ssh.ConnMetadata, key ssh.PublicKey) (*ssh.Permissions, error) {
	return &ssh.Permissions{
		Extensions: map[string]string{"name": "1"},
	}, nil
}

func (t *SSHTunneld) LoadAuthorizedKeys(authfilePath string) error {
	keys := map[string]ClientIdentity{}
	fileContent, err := ioutil.ReadFile(authfilePath)
	if err != nil {
		return fmt.Errorf("could not read authfile: %w", err)
	}

	for len(fileContent) > 0 {
		pubkey, comment, options, rest, err := ssh.ParseAuthorizedKey(fileContent)
		groups := ""
		for _, opt := range options {
			if strings.HasPrefix(opt, "groups=") {
				opt = opt[len("groups=")+1:]
				groups = strings.TrimPrefix(strings.TrimSuffix(opt, "\""), "\"")
			}
		}
		if err != nil {
			fileContent = rest
			continue
		}
		keys[string(pubkey.Marshal())] = ClientIdentity{Name: comment, Groups: strings.Split(groups, ",")}

		fileContent = rest
	}

	t.authmutex.Lock()
	defer t.authmutex.Unlock()
	t.authorisedKeys = keys

	return nil
}
