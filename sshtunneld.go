package sshtunneld

import (
	"crypto/rand"
	"crypto/rsa"
	"fmt"
	"io/ioutil"
	"net"
	"strings"
	"sync"
	"time"

	"github.com/go-logr/logr"
	"golang.org/x/crypto/ssh"
)

type SSHTunneld struct {
	log       logr.Logger
	sshConfig *ssh.ServerConfig

	connectionHandler ConnectionHandler

	authorisedKeys map[string]ClientIdentity
	authmutex      sync.Mutex
}

type ConnectionHandler interface {
	IsConnectionAllowed(clientId ClientIdentity, destname string, destPort uint32) bool
	EstablishConnection(clientId ClientIdentity, destname string, destPort uint32) (net.Conn, error)
	HandleConnection(clientId ClientIdentity, destName string, destPort uint32, channel ssh.Channel, conn net.Conn)
}

type DefaultConnectionHandler struct{}

func (d *DefaultConnectionHandler) IsConnectionAllowed(clientId ClientIdentity, destname string, destPort uint32) bool {
	return true
}
func (d *DefaultConnectionHandler) EstablishConnection(clientId ClientIdentity, destName string, destPort uint32) (net.Conn, error) {
	return net.Dial("tcp", fmt.Sprintf("%v:%v", destName, destPort))
}
func (d *DefaultConnectionHandler) HandleConnection(clientId ClientIdentity, destName string, destPort uint32, channel ssh.Channel, conn net.Conn) {
	ServeConnectionWithTimeout(channel, conn, 2*time.Minute)
}

type ClientIdentity struct {
	Name   string
	Groups []string
}

func NewWithKeyFiles(log logr.Logger, hostkeyPath string, authorizedKeysPath string, connectionHandler ConnectionHandler) (*SSHTunneld, error) {
	privateBytes, err := ioutil.ReadFile(hostkeyPath)
	if err != nil {
		return nil, fmt.Errorf("could not read %v: %w", hostkeyPath, err)
	}
	private, err := ssh.ParsePrivateKey(privateBytes)
	if err != nil {
		return nil, fmt.Errorf("could not parse file %v: %w", hostkeyPath, err)
	}
	cfg := &ssh.ServerConfig{}
	cfg.AddHostKey(private)

	tunnel, err := New(log, cfg, connectionHandler)
	if err != nil {
		return nil, err
	}

	err = tunnel.LoadAuthorizedKeys(authorizedKeysPath)
	if err != nil {
		return nil, fmt.Errorf("could not load authorized keys %v: %w", authorizedKeysPath, err)
	}
	return tunnel, nil
}

func New(log logr.Logger, cfg *ssh.ServerConfig, connectionHandler ConnectionHandler) (*SSHTunneld, error) {
	if cfg == nil {
		cfg = new(ssh.ServerConfig)

		key, err := rsa.GenerateKey(rand.Reader, 2048)
		if err != nil {
			return nil, err
		}
		signer, err := ssh.NewSignerFromKey(key)
		if err != nil {
			return nil, err
		}
		cfg.AddHostKey(signer)
	}
	if cfg.ServerVersion == "" {
		cfg.ServerVersion = "SSH-2.0-SSHTUNNELD"
	}

	if connectionHandler == nil {
		connectionHandler = &DefaultConnectionHandler{}
	}
	tunnel := &SSHTunneld{
		log:       log,
		sshConfig: cfg,

		connectionHandler: connectionHandler,
	}

	if cfg.PublicKeyCallback == nil {
		cfg.PublicKeyCallback = tunnel.publicKeyCallback
	}

	return tunnel, nil
}

func (t *SSHTunneld) Listen(network string, address string) error {
	listener, err := net.Listen(network, address)
	if err != nil {
		return err
	}

	for {
		conn, err := listener.Accept()
		if err != nil {
			t.log.Info("connection could not be accepted", "error", err)
			continue
		}
		// Before use, a handshake must be performed on the incoming net.Conn.
		sConn, chans, reqs, err := ssh.NewServerConn(conn, t.sshConfig)
		if err != nil {
			t.log.Info("could not establish SSH connection", "error", err)
			continue
		}

		// The incoming Request channel must be serviced.
		go ssh.DiscardRequests(reqs)
		go t.handleServerConn(ClientIdentity{sConn.Permissions.Extensions["name"], strings.Split(sConn.Permissions.Extensions["groups"], ",")}, chans)
	}
}
