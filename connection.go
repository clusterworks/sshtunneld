package sshtunneld

import (
	"fmt"
	"io"
	"net"
	"sync"
	"time"

	"github.com/go-logr/logr"
	"golang.org/x/crypto/ssh"
)

type channelOpenDirectMsg struct {
	Raddr string
	Rport uint32
	//	Laddr string
	//	Lport uint32
}

func (t *SSHTunneld) handleServerConn(clientId ClientIdentity, chans <-chan ssh.NewChannel) {
	ulog := t.log.WithValues("userId", clientId.Name)
	for newChan := range chans {
		channelType := newChan.ChannelType()
		switch channelType {
		case "session":
			schan, req, err := newChan.Accept()

			if err == nil {
				go pseudoShell(ulog, schan, req)
			} else {
				ulog.Error(err, "could not accept session channel")
			}
		case "direct-tcpip":
			var connData channelOpenDirectMsg
			ssh.Unmarshal(newChan.ExtraData(), &connData)
			tcpLog := ulog.WithValues("host", connData.Raddr, "port", connData.Rport)
			tcpLog.V(1).Info("direct-tcpip")
			if !t.connectionHandler.IsConnectionAllowed(clientId, connData.Raddr, connData.Rport) {
				tcpLog.Info("not allowed")
				newChan.Reject(ssh.Prohibited, "not allowed")
			} else {
				conn, err := t.connectionHandler.EstablishConnection(clientId, connData.Raddr, connData.Rport)
				if err != nil {
					tcpLog.Error(err, "connection could not be established")
					newChan.Reject(ssh.ConnectionFailed, fmt.Sprintf("%v", err))
					continue
				} else {
					tcpLog.V(1).Info("start forwarding...")
					tunChannel, req, err := newChan.Accept()
					if err == nil {
						go ssh.DiscardRequests(req)
						go t.connectionHandler.HandleConnection(clientId, connData.Raddr, connData.Rport, tunChannel, conn)
					}
				}
			}
		default:
			ulog.V(2).Info("unknown channel", "channelType", channelType)
			newChan.Reject(ssh.UnknownChannelType, channelType)
		}
	}
}

func pseudoShell(log logr.Logger, channel ssh.Channel, in <-chan *ssh.Request) error {
	defer channel.Close()

	channel.Write([]byte("This server does not support any commands, run ssh with -N\r\n"))

	for req := range in {
		switch req.Type {
		default:
			log.V(2).Info("unrecognized ssh request", "type", req.Type, "wantReplay", req.WantReply)
			req.Reply(false, nil) // unhandled; tell them so
		case "pty-req":
			req.Reply(false, nil)
		case "shell":
			req.Reply(true, nil)
		case "env":
			req.Reply(true, nil)
		case "exec":
			req.Reply(false, nil)
		}
	}

	return nil
}

func ServeConnectionWithTimeout(cssh ssh.Channel, conn net.Conn, timeout time.Duration) {
	close := func() {
		cssh.Close()
		conn.Close()
	}

	var once sync.Once
	go func() {
		copyTimeout(cssh, conn, func() {
			conn.SetDeadline(time.Now().Add(timeout))
		})
		once.Do(close)
	}()
	go func() {
		copyTimeout(conn, cssh, func() {
			conn.SetDeadline(time.Now().Add(timeout))
		})
		once.Do(close)
	}()
}

type TimeoutFunc func()

// Changed from pkg/io/io.go copyBuffer
func copyTimeout(dst io.Writer, src io.Reader, timeout TimeoutFunc) (written int64, err error) {
	buf := make([]byte, 32*1024)

	for {
		nr, er := src.Read(buf)
		if nr > 0 {
			timeout()

			nw, ew := dst.Write(buf[0:nr])
			if nw > 0 {
				written += int64(nw)
			}
			if ew != nil {
				err = ew
				break
			}
			if nr != nw {
				err = io.ErrShortWrite
				break
			}
			timeout()
		}
		if er != nil {
			if er != io.EOF {
				err = er
			}
			break
		}
	}
	return written, err
}
